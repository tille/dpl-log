2025-01-01
  - BotD: oggfwd, aconnectgui, python-pyramid-tm
  - Reviving Huawai sponsorship

2025-01-02
  - BotD: libdshconfig
  - Drafting Bits

2025-01-03
  - BotD: libuninum
  - Drafting Bits

2025-01-04
  - BotD: debroster
  - Sending Bits
  - Sad to learn Vorlon died
  - Ping release team
  - Publicity team

2025-01-05
  - BotD: ruby-rails-assets-markdown-it-sub

2025-01-06
  - BotD: pyramid-jinja2, flvstreamer
  - ITS+NMU discussion
  - tag2upload delegation discussion

2025-01-07
  - BotD: aspell-pl
  - Drafting outreachy team delegation

2025-01-08
  - BotD: clog
  - Publicity team meeting
    https://meetbot.debian.net/debian-publicity/2025/debian-publicity.2025-01-08-16.00.log.html

2025-01-09
  - BotD: libjs-jquery-hotkeys
  - Outreachy team delegation

2025-01-10
  - BotD: xbubble
  - Discussing leaving Twitter/X with publicity team

2025-01-11
  - BotD: netsend
  
2025-01-12
  - Discussing tag2upload
  - Discussing ftpmaster
  - Any chance to get funding from https://www.sovereign.tech
  - BotD: littlewizard
  - Keep on discussing leaving X

2025-01-13
  - Discussing tag2upload
  - Keep on discussing Sovereing Tech Fund and adding it to bits draft
  - BotD: sat4j

2025-01-14
  - Discussing + Delegation tag2upload
  - BotD: localehelper
  - Keep on discussing Sovereing Tech Fund
  - Ping about SPI documents + recruiting new ftpmasters

2025-01-15
  - Who is using Debian: MediaWiki
  - Tag2upload
  - Year of code reviews: More input for ITS 2.0 project for DebCamp
  - BotD: ogamesim
  - Vorlon obituary text

2025-01-16
  - Vorlon obituary text
  - Ping release team
  - BotD: debpear

2025-01-17 (VAC)
  - Vorlon obituary text
  - BotD: xshogi

2025-01-18 (VAC)
  - Mini Debconf Maceió 2025
  - BotD: cvsweb

2025-01-19 (VAC)
  - Mini Debconf Maceió 2025
  - BotD: rgbpaint

2025-01-20
  - Mini Debconf Maceió 2025
  - Community team issue in Python team
  - BotD: cereal

2025-01-21
  - BotD: barada-pam, wrapsrv
  - FOSSASIA preparation

2025-01-22
  - Discussing leaving X with publicity team
  - Community team issue in Python team
  - BotD: anomaly, xcb-util-renderutil, xcb-util-keysyms
  - tag2upload delegates request

2025-01-23
  - Discussing leaving X with publicity team on mailing list
  - Discussing leaving X with debconf-team
  - BotD: pnmixer
  - Seek for OSI Affiliate Member Representative for Debian

2025-01-24
  - Discussing leaving X (IRC + mail)
  - BotD: pgdbf

2025-01-25
  - Debian Open Source Initiative Affiliation
  - BotD: python-librtmp

2025-01-26
  - Discussing leaving X
  - BotD: tempest-for-eliza, flobopuyo, yaret

2025-01-27
  - BotD: searchandrescue(-data)
  - Community team issue in Python team
  - Approve riscv64 related hardware

2025-01-28
  - BotD: aggregate
  - Discussing leaving X

2025-01-29
  - BotD: mysecureshell
  - Debian left X
  - Debian Open Source Initiative Affiliation
  - Fund raising idea for Southern California Linux Expo
  - Problematic maintainer reported in private
  - Approve money for FOSDEM and Bug squashing party Vienna
 
2025-01-30
  - BotD: hyphen-ru
  - Discussion about decision to stop posting on X

2025-01-31
  - BotD: dotconf
  - Donation from https://www.cyon.ch/ (Ch. Schubnell)
  - Community team issue in Python team
  - Drafting Bits
