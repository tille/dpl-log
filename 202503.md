2025-03-01
  - BotD: trousers
  - More drafting Bits

2025-03-02
  - Ask for review on Bits
  - BotD: gbdfed

2025-03-03
  - Continue slides for FOSSASIA talk
  - BotD: rdtool

2025-03-04
  - Sent Bits
  - Continue slides for FOSSASIA talk
  - Discuss ftpmaster team issues
  - DebConf25 Budget request
  - BotD: ruby-cocoon

2025-03-05
  - Continue slides for FOSSASIA talk
  - Discuss ftpmaster team issues
  - BotD: numdiff

2025-03-06
  - Continue slides for FOSSASIA talk
  - Discuss ftpmaster team issues
  - BotD: libhtml-element-extended-perl
  - Discussing DebConf25 Budget 

2025-03-07
  - Discuss ftpmaster team issues
  - BotD: django-tastypie
  - Discussing DebConf25 Budget 
  - Tweak some Debian advertising into a local TV report about my tree planting ;-)
